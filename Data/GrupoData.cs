using System.Data.SqlClient; // SqlConnection, SqlCommand, SqlDataReader
using System.Collections.Generic;
using ContatoWeb.Models;
using System;

namespace ContatoWeb.Data
{
    public class GrupoData : IDisposable
    {
        private SqlConnection connection;

        public GrupoData()
        {
            string strConn = @"Data Source=localhost;
                                Initial Catalog=BDContato;
                                Integrated Security=true"; // false
                                // User Id = <usuario>; Password = <senha>

            connection = new SqlConnection(strConn);
            connection.Open();
        }

        public List<Grupo> Read()
        {
            var lista = new List<Grupo>();

            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM Grupo";

            var reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                var grupo = new Grupo();
                grupo.GrupoId = reader.GetInt32(0);
                grupo.Nome = reader.GetString(1);

                lista.Add(grupo);
            }

            return lista;
        }

        public void Dispose()
        {
            connection.Close();
        }

        public void Create(Grupo grupo)
        {
            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Grupo VALUES (@nome)";

            cmd.Parameters.AddWithValue("@nome", grupo.Nome);

            cmd.ExecuteNonQuery();
        }

        public Grupo Read(int id)
        {
            Grupo grupo = null;

            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT * FROM Grupo
                                WHERE GrupoId = @id";

            cmd.Parameters.AddWithValue("@id", id);

            var reader = cmd.ExecuteReader();

            if(reader.Read())
            {
                grupo = new Grupo {
                    GrupoId = (int)reader["GrupoId"],
                    Nome = reader["Nome"] as string
                };
            }

            return grupo;
        }

        public void Update(Grupo grupo)
        {
            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"UPDATE Grupo SET Nome = @nome
                                WHERE GrupoId = @id";

            cmd.Parameters.AddWithValue("@id", grupo.GrupoId);
            cmd.Parameters.AddWithValue("@nome", grupo.Nome);            

            cmd.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "DELETE FROM Grupo  WHERE GrupoId = @id";

            cmd.Parameters.AddWithValue("@id", id);           

            cmd.ExecuteNonQuery();
        }

    }
}

// GrupoData data = new GrupoData();
// data.Read();