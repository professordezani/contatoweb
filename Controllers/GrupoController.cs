using Microsoft.AspNetCore.Mvc;
using ContatoWeb.Data;
using ContatoWeb.Models;

namespace ContatoWeb.Controllers
{
    public class GrupoController : Controller
    {
        public IActionResult Index()
        {
            // using(var data = new GrupoData())
            // {
            //     var lista = data.Read();            
            //     return View(lista); // model => Lista<Grupo>
            
            // } // Dispose()

            using(var data = new GrupoData())           
                return View(data.Read());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Grupo grupo)
        {
            if(!ModelState.IsValid)
                return View(grupo);

            using(var data = new GrupoData())
                data.Create(grupo);
                
            return RedirectToAction("Index");
        }

        public IActionResult Update(int id)
        {
            using(var data = new GrupoData())
                return View(data.Read(id));
        }

        [HttpPost]
        public IActionResult Update(int id, Grupo grupo)
        {
            grupo.GrupoId = id;

            if(!ModelState.IsValid)
                return View(grupo);

            using(var data = new GrupoData())
                data.Update(grupo);

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            using(var data = new GrupoData())
                data.Delete(id);

            return RedirectToAction("Index");
        }
    }
}