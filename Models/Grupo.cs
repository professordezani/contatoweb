using System.ComponentModel.DataAnnotations;

namespace ContatoWeb.Models
{
    public class Grupo
    {
        public int GrupoId { get; set; }

        [Required(ErrorMessage="Campo nome obrigatório")]
        public string Nome { get; set; }
    }
}