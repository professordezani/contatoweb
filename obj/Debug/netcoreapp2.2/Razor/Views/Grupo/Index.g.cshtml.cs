#pragma checksum "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6a4ad904193f91f192fd4c52ec971342af2dcda2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Grupo_Index), @"mvc.1.0.view", @"/Views/Grupo/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Grupo/Index.cshtml", typeof(AspNetCore.Views_Grupo_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\_ViewImports.cshtml"
using ContatoWeb.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6a4ad904193f91f192fd4c52ec971342af2dcda2", @"/Views/Grupo/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7d95122bf397b41df78e7dc97b69a4f308b1fbc5", @"/Views/_ViewImports.cshtml")]
    public class Views_Grupo_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Grupo>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(20, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
  
    Layout = "../Shared/_Layout.cshtml";
    ViewBag.Title = "Lista de Grupos";

#line default
#line hidden
            BeginContext(111, 15, true);
            WriteLiteral("\r\n    <table>\r\n");
            EndContext();
#line 9 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
         foreach (var grupo in Model)
        {

#line default
#line hidden
            BeginContext(176, 37, true);
            WriteLiteral("           <tr>\r\n                <td>");
            EndContext();
            BeginContext(214, 13, false);
#line 12 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
               Write(grupo.GrupoId);

#line default
#line hidden
            EndContext();
            BeginContext(227, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(255, 10, false);
#line 13 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
               Write(grupo.Nome);

#line default
#line hidden
            EndContext();
            BeginContext(265, 51, true);
            WriteLiteral("</td>\r\n                <td>\r\n                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 316, "\"", 351, 2);
            WriteAttributeValue("", 323, "/Grupo/Update/", 323, 14, true);
#line 15 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
WriteAttributeValue("", 337, grupo.GrupoId, 337, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(352, 80, true);
            WriteLiteral(">Editar</a>\r\n                </td>\r\n                <td>\r\n                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 432, "\"", 467, 2);
            WriteAttributeValue("", 439, "/Grupo/Delete/", 439, 14, true);
#line 18 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
WriteAttributeValue("", 453, grupo.GrupoId, 453, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(468, 56, true);
            WriteLiteral(">Apagar</a>\r\n                </td>\r\n            </tr> \r\n");
            EndContext();
#line 21 "c:\Users\MAQ01LAB04\Documents\Henrique\ContatoWeb\Views\Grupo\Index.cshtml"
        } 

#line default
#line hidden
            BeginContext(536, 57, true);
            WriteLiteral("    </table>\r\n    <a href=\"/Grupo/Create\">Adicionar</a>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Grupo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
